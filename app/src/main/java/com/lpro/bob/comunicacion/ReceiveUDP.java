package com.lpro.bob.comunicacion;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.List;

public class ReceiveUDP extends AsyncTask {
    private Context context;
    private DatagramSocket udpSocket;
    private InetAddress broadcastIP, ourIP;
    private final static int port = 9995;
    private String message;
    private byte[] data;
    private DatagramPacket sentPacket, receivedPacket;

    public ReceiveUDP(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        getOurIPs();
        try {
            Log.d("Configuration", "onPreExecute");
            udpSocket = new DatagramSocket(9995, broadcastIP);
            udpSocket.setBroadcast(true);
            message = "app*connect";
            data = new byte[1024];
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Object doInBackground(Object[] params) {
        Log.d("Configuration", "doInBackground");

        sentPacket = new DatagramPacket(message.getBytes(), message.length(), broadcastIP, port);
        receivedPacket = new DatagramPacket(data, 1024);
        try {
            Log.d("Configuration", "Sending...");
            udpSocket.send(sentPacket);
            Log.d("Configuration","Enviado");
            udpSocket.receive(receivedPacket);
            Log.d("Configuration", "IP is " + receivedPacket.getAddress().getHostAddress());

            SharedPreferences sharedPref = context.getSharedPreferences("sharedPreferences", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("ip", receivedPacket.getAddress().getHostAddress());
            editor.commit();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
    }

    private void getOurIPs() {

        try {

            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());

            for (NetworkInterface intf : interfaces) {

                for (InterfaceAddress interfaceAddress : intf.getInterfaceAddresses()) {

                    InetAddress auxBroadcastIP = interfaceAddress.getBroadcast();

                    InetAddress auxIP = interfaceAddress.getAddress();

                    if ((auxIP instanceof Inet4Address) && !auxIP.isLoopbackAddress() && (auxBroadcastIP != null)) {

                        broadcastIP = auxBroadcastIP;

                        ourIP = auxIP;

                        Log.d("Addresses", "IP: " + ourIP.getHostAddress() + "; Broadcast: " + broadcastIP.getHostAddress());

                    }
                }

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
