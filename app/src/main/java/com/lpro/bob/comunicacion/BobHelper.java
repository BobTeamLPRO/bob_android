package com.lpro.bob.comunicacion;

import android.content.Context;
import android.content.SharedPreferences;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import java.util.ArrayList;


public class BobHelper extends NotificationListenerService {
    private String TAG = "BobHelper";
    private String message = "";
    SharedPreferences sharedPref;
    Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        Log.d(TAG, "On create, Bob Helper");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {

        if (sbn.getNotification().category != null) {
            if (sbn.getNotification().category.equals("msg")) {
                //Whatsapp
                if (sbn.getPackageName().split("\\.")[sbn.getPackageName().split("\\.").length - 1].equals("whatsapp")) {
                    Log.d("Notification", "*********** NUEVO WHATSAPP");
                    if (sbn.getNotification().extras.getCharSequenceArray("android.textLines") != null) {
                        CharSequence[] cs = sbn.getNotification().extras.getCharSequenceArray("android.textLines");
                        message = "notify*whatsapp*" + cs[cs.length - 1].toString().split("\\:")[0].trim() + "*" + cs[cs.length - 1].toString().split("\\:")[1].trim();
                        Log.d("Notification", message);
                        new SendToBob(context, message).execute();
                    } else {
                        message = "notify*whatsapp*" + sbn.getNotification().extras.getString("android.text").split("\\:")[1].trim() + "*" + sbn.getNotification().extras.getString("android.text").split("\\:")[2].trim();
                        Log.d("Notification", message);
                        new SendToBob(context, message).execute();
                    }
                } else {
                    //Telegram
                    if (sbn.getPackageName().split("\\.")[sbn.getPackageName().split("\\.").length - 1].equals("messenger")) {
                        Log.d("Notification", "*********** NUEVO TELEGRAM");
                        if (sbn.getNotification().tickerText != null) {
                            message = "notify*telegram*" + sbn.getNotification().tickerText.toString().split("\\:")[0].trim() + "*" + sbn.getNotification().tickerText.toString().split("\\:")[1].trim();
                            Log.d("Notification", message);
                            new SendToBob(context, message).execute();
                        }
                    } else {
                        //SMS
                        message = "notify*sms*" + sbn.getNotification().tickerText.toString().split("\\:")[0].trim() + "*" + sbn.getNotification().tickerText.toString().split("\\:")[1].trim();
                        Log.d("Notification", message);
                        new SendToBob(context, message).execute();
                    }
                }
            } else {
                if (sbn.getNotification().category.equals("email")) {
                    Log.d("Notification", "*********** NUEVO CORREO");
                    if (sbn.getNotification().extras.getString("android.bigText") != null) {
                        message = "notify*mail*" + sbn.getNotification().extras.getString("android.title") + "*" + sbn.getNotification().extras.getString("android.bigText");
                        Log.d("Notification", message);
                        new SendToBob(context, message).execute();
                    } else {
                        CharSequence[] cs = sbn.getNotification().extras.getCharSequenceArray("android.textLines");
                        ArrayList<String> messages = new ArrayList<String>();
                        for (CharSequence aux : cs) {
                            message = "notify*mail*more*" + aux.toString();
                            Log.d("Notification", "notify*mail*more*" + aux.toString());
                        }
                        new SendToBob(context, message).execute();
                    }
                } else {
                    Log.d("Notification", "Notificacion no contemplada" + sbn.getPackageName());
                    message = "notify*" + sbn.getNotification().extras.getString("android.title");

                    new SendToBob(context, message).execute();
                    
                    if (sbn.getNotification().category.equals("event")) {

                    } else {
                        if (sbn.getNotification().category.equals("social")) {

                        } else {
                            if (sbn.getNotification().category.equals("status")) {

                            } else {
                                if (sbn.getNotification().category.equals("alarm")) {

                                } else {
                                    if (sbn.getNotification().category.equals("call")) {

                                    } else {

                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
        Log.d("BobHelper", "Llega al final");

    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {

    }


}
