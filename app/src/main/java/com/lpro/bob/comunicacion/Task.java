package com.lpro.bob.comunicacion;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;

import com.lpro.bob.interfaz.ConfiguracionActivity;
import com.lpro.bob.interfaz.SearchPhoneActivity;
import com.lpro.bob.interfaz.SendMailActivity;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

public class Task extends Thread {
    private Socket client;
    private Context context;
    private ConfiguracionActivity configuracion;

    public Task(Socket client, Context context, ConfiguracionActivity configuracion) {
        this.client = client;
        this.context = context;
        this.configuracion = configuracion;
    }

    public void run() {
        try {
            //read input stream
            DataInputStream dis2 = new DataInputStream(client.getInputStream());
            InputStreamReader disR2 = new InputStreamReader(dis2);
            BufferedReader br = new BufferedReader(disR2);
            try {
                String message = br.readLine();
                String[] splited = message.split("\\*");
                Log.i("Thread", "Mensaje recibido: " + message);
                switch (splited[0]) {
                    case "mail":
                        Intent mail = new Intent(context, SendMailActivity.class);
                        mail.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(mail);
                        break;
                    case "search":
                        Intent buscar = new Intent(context, SearchPhoneActivity.class);
                        buscar.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(buscar);
                        break;
                    case "musica":
                        String ip = "192.168.0.29";
                        int port = 9997;
                        Log.wtf("IP", ip);
                        Log.wtf("Puerto", String.valueOf(port));
                        Socket connectSocket = new Socket(ip, port);
                        Log.wtf("Aquí", "Conectado");
                        if (connectSocket.isConnected()) {
                            File file = new File(
                                    Environment.getExternalStorageDirectory(),
                                    "Piratas.mp3");
                            long length = file.length();
                            byte[] bytes = new byte[1024];
                            InputStream in = new FileInputStream(file);
                            OutputStream out = connectSocket.getOutputStream();
                            int count = 0;
                            while ((count = in.read(bytes)) > 0) {
                                out.write(bytes, 0, count);
                            }
                            Log.wtf("Cuak", count + "");
                        }
                        break;
                    case "config":
                        configuracion.notifyMessageReceived(message);
                        break;
                    default:
                        break;
                }

            } catch (NullPointerException npe) {
                npe.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
