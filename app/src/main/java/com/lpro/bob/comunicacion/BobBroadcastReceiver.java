package com.lpro.bob.comunicacion;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BobBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)){
            Intent serviceIntent = new Intent(context, com.lpro.bob.comunicacion.BobHelper.class);
            context.startService(serviceIntent);
            Log.wtf("Startup", "Se va a ejecutar el servicio BobHelper");
            serviceIntent = new Intent(context, com.lpro.bob.comunicacion.BobServer.class);
            context.startService(serviceIntent);
            Log.wtf("Startup", "Se va a ejecutar el servicio BobServer");
        }
    }
}
