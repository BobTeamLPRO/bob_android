package com.lpro.bob.comunicacion;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

public class SendToBob extends AsyncTask<Void, Void, Integer> {
    private String ip;
    private int port;
    private String message;
    private Socket connectSocket;
    private Context context;
    private int cont;

    public SendToBob(Context context, String message) {
        this.message = message;
        this.context = context;
        SharedPreferences sharedPref = context.getSharedPreferences("sharedPreferences", Context.MODE_PRIVATE);
        ip = sharedPref.getString("ip", "192.168.2.5");
        port = sharedPref.getInt("port", 9998);
    }

    public SendToBob(String ip, int port, String message) {
        super();
        this.ip = ip;
        this.port = port;
        this.message = message;
    }


    @Override
    protected void onPreExecute() {
        if (cont == 0) {
            super.onPreExecute();
            Log.d("SendToBob", "onPreExecute");
            try {
                Log.d("SendToBob", ip);
                Log.d("SendToBob", String.valueOf(port));
                Log.d("SendToBob", message);
                connectSocket = new Socket(); // Establecemos el Socket
                connectSocket.connect(new InetSocketAddress(ip, port));
                Log.d("SendToBob", "Conectado");
                if (connectSocket.isConnected()) { // Si conseguimos conectarnos
                    OutputStream flujo = connectSocket.getOutputStream();
                    flujo.write(message.getBytes());
                } else {
                    Log.d("SendToBob", "No estamos conectados");
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.d("Excepcion", "Ha ocurrido una excepcion: " + e);
            }
            cont = 1;
        }
    }

    @Override
    protected Integer doInBackground(Void... params) {
        /*try {
            Log.d("SendToBob", ip);
            Log.d("SendToBob", String.valueOf(port));
            Log.d("SendToBob", message);
            connectSocket = new Socket(); // Establecemos el Socket
            connectSocket.connect(new InetSocketAddress(ip, port));
            Log.d("SendToBob", "Conectado");
            if (connectSocket.isConnected()) { // Si conseguimos conectarnos
                OutputStream flujo = connectSocket.getOutputStream();
                flujo.write(message.getBytes());
            } else {
                Log.d("SendToBob", "No estamos conectados");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("Excepcion", "Ha ocurrido una excepcion: " + e);
        }
*/
        return 1;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);
        cont = 0;
    }
}