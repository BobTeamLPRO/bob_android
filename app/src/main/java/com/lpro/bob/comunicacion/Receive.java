package com.lpro.bob.comunicacion;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.lpro.bob.interfaz.ConfiguracionActivity;

import java.net.ServerSocket;
import java.net.Socket;

public class Receive extends AsyncTask<Void, Void, Integer> {

    public final static int port = 9999;
    private static ServerSocket server;
    private static Socket client;
    private Task task;
    private Context context;
    private ConfiguracionActivity configuracion;

    public Receive(Context context) {
        this.context = context;
    }

    public Receive(Context context, ConfiguracionActivity configuracion) {
        this.context = context;
        this.configuracion = configuracion;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        try {
            server = new ServerSocket(port);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected Integer doInBackground(Void... params) {
        try {
            while (true) {
                Log.i("Server", "Esperando peticiones...");
                client = server.accept();
                Log.i("Server", "Conexion establecida. Iniciando tareas...");
                task = new Task(client, context, configuracion);
                task.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
}