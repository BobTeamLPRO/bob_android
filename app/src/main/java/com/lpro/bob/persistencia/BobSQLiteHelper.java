package com.lpro.bob.persistencia;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import com.lpro.bob.persistencia.Contrato.Dispositivos;

public class BobSQLiteHelper extends SQLiteOpenHelper {

    static final int VERSION = 1;

    static final String NOMBRE_BD = "bob.db";

    interface Tablas {
        String DISPOSITIVOS = "dispositivos";
    }

    public BobSQLiteHelper(Context context) {
        super(context, NOMBRE_BD, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE " + Tablas.DISPOSITIVOS + "("
                        + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                        + Dispositivos.IDENTIFICADOR + " TEXT NOT NULL,"
                        + Dispositivos.UBICACION + " TEXT NOT NULL,"
                        + Dispositivos.MAC + " TEXT NOT NULL,"
                        + Dispositivos.TIPO + " TEXT NOT NULL)");

        /** Insertamos valores de muestra **/
        ContentValues valores = new ContentValues();
        valores.put(Dispositivos.IDENTIFICADOR, Dispositivos.generarIdDispositivo());
        valores.put(Dispositivos.UBICACION, "Pasillo");
        valores.put(Dispositivos.MAC, "B8:27:EB:49:91:1F");
        valores.put(Dispositivos.TIPO, "Bob");
        db.insertOrThrow(Tablas.DISPOSITIVOS, null, valores);

        ContentValues valores2 = new ContentValues();
        valores.put(Dispositivos.UBICACION, "Mesa del pasillo 1");
        valores.put(Dispositivos.MAC, "B8:27:EB:FC:D5:81");
        valores.put(Dispositivos.TIPO, "MiniBob");
        db.insertOrThrow(Tablas.DISPOSITIVOS, null, valores);

        ContentValues valores3 = new ContentValues();
        valores.put(Dispositivos.UBICACION, "Mesa del pasillo 2");
        valores.put(Dispositivos.MAC, "C0:4A:00:16:2D:3D");
        valores.put(Dispositivos.TIPO, "MiniBob");
        db.insertOrThrow(Tablas.DISPOSITIVOS, null, valores);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            db.execSQL("DROP TABLE IF EXISTS " + Tablas.DISPOSITIVOS);
        } catch (SQLiteException e) {
            // Manejo de excepciones
        }
        onCreate(db);
    }

}
