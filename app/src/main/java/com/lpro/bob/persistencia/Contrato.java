package com.lpro.bob.persistencia;

import android.net.Uri;

import java.util.UUID;

/**
 * Contrato con la estructura de la base de datos y forma de las URIs
 */
public class Contrato {

    interface ColumnasDispositivos{
        String IDENTIFICADOR = "identificador";
        String UBICACION  = "ubicacion";
        String MAC = "mac";
        String TIPO = "tipo";
    }


    /**
     * Representa la identificación única del Content Provider sobre otros. Se usa el
     * nombre del paquete donde se encuentra el componente para asegurar unicidad
     * **/
    public final static String AUTORIDAD = "com.lpro.bob.interfaz";

    /**
     * URI base. Debe empezar por la cadena content:// que determina el esquema usado
     * para indicarle al framework de Android que la URI se refiere a un content provider
     * **/
    public final static Uri URI_CONTENIDO_BASE = Uri.parse("content://" + AUTORIDAD);


    /**
     * Controlador de la tabla "dispositivo"
     */
    public static class Dispositivos implements ColumnasDispositivos {

        public static final Uri URI_CONTENIDO =
                URI_CONTENIDO_BASE.buildUpon().appendPath(RECURSO_DISPOSITIVO).build();

        public final static String MIME_RECURSO =
                "vnd.android.cursor.item/vnd." + AUTORIDAD + "/" + RECURSO_DISPOSITIVO;

        public final static String MIME_COLECCION =
                "vnd.android.cursor.dir/vnd." + AUTORIDAD + "/" + RECURSO_DISPOSITIVO;


        /**
         * Construye una {@link Uri} para el {@link #IDENTIFICADOR} solicitado.
         */
        public static Uri construirUriAlquiler(String idApartamento) {
            return URI_CONTENIDO.buildUpon().appendPath(idApartamento).build();
        }

        public static String generarIdDispositivo() {
            return "A-" + UUID.randomUUID();
        }

        public static String obtenerIdDispositivo(Uri uri) {
            return uri.getLastPathSegment();
        }
    }

    // Recursos
    public final static String RECURSO_DISPOSITIVO = "dispositivos";

}
