package com.lpro.bob.persistencia;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.lpro.bob.persistencia.BobSQLiteHelper.Tablas;
import com.lpro.bob.persistencia.Contrato.Dispositivos;

/***
 * Clase que encapsula una implementación SQLite a través de un mecanismo de identificadores (URIs)
 * para que otras aplicaciones no sepan nada sobre su estructura. Sin embargo se proporcionan
 * las modalidades necesarias para consultar, insertar, eliminar y actualizar los datos.
 */
public class ProviderDispositivos extends ContentProvider {

    // Comparador de URIs
    public static final UriMatcher uriMatcher;

    // Casos
    public static final int DISPOSITIVOS = 100;
    public static final int IDENTIFICADOR = 101;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(Contrato.AUTORIDAD, "dispositivos", DISPOSITIVOS);
        uriMatcher.addURI(Contrato.AUTORIDAD, "dispositivos/*", IDENTIFICADOR);
    }

    private BobSQLiteHelper bd;
    private ContentResolver resolver;


    @Override
    public boolean onCreate() {
        bd = new BobSQLiteHelper(getContext());
        resolver = getContext().getContentResolver();
        return true;
    }

    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            case DISPOSITIVOS:
                return Dispositivos.MIME_COLECCION;
            case IDENTIFICADOR:
                return Dispositivos.MIME_RECURSO;
            default:
                throw new IllegalArgumentException("Tipo desconocido: " + uri);
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        // Obtener base de datos
        SQLiteDatabase db = bd.getWritableDatabase();
        // Comparar Uri
        int match = uriMatcher.match(uri);

        Cursor c;
        Log.i("Provider","MATCH: " + match);
        switch (match) {
            case DISPOSITIVOS:
                // Consultando todos los registros
                c = db.query(Tablas.DISPOSITIVOS, projection,
                        selection, selectionArgs,
                        null, null, sortOrder);
                c.setNotificationUri(resolver, Dispositivos.URI_CONTENIDO);
                break;
            case IDENTIFICADOR:
                // Consultando un solo registro basado en el Id del Uri
                String idApartamento = Dispositivos.obtenerIdDispositivo(uri);
                c = db.query(Tablas.DISPOSITIVOS, projection,
                        Dispositivos.IDENTIFICADOR + "=" + "\'" + idApartamento + "\'"
                                + (!TextUtils.isEmpty(selection) ?
                                " AND (" + selection + ')' : ""),
                        selectionArgs, null, null, sortOrder);
                c.setNotificationUri(resolver, uri);
                break;
            default:
                throw new IllegalArgumentException("URI no soportada: " + uri);
        }
        return c;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        return 0;
    }
}
