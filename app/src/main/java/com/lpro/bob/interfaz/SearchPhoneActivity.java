package com.lpro.bob.interfaz;

import android.app.Activity;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.lpro.bob.R;

public class SearchPhoneActivity extends Activity {
    private Ringtone r;
    private Button detenerButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_phone);
        buscarTelefono();

        detenerButton = (Button) findViewById(R.id.btn_detener_busqueda);
        detenerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pararBuscarTelefono();
            }
        });

    }

    private void buscarTelefono() {

        Uri notificacion = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        r = RingtoneManager.getRingtone(getApplicationContext(), notificacion);
        r.setStreamType(AudioManager.STREAM_ALARM);
        r.play();
    }

    private void pararBuscarTelefono() {
        Log.wtf("Parar buscar telefono", "Estamos en parando telefono");
        r.stop();
        super.onBackPressed();
    }
}