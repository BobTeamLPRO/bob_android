package com.lpro.bob.interfaz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.lpro.bob.R;

public class AjustesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Display the fragment as the main content.
//        setContentView(R.layout.activity_ajustes);

        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new AjustesFragment())
                .commit();
    }
}
