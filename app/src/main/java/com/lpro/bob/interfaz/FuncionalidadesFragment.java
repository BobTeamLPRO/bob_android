package com.lpro.bob.interfaz;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import com.lpro.bob.R;

public class FuncionalidadesFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.funcionalidades);
    }
}
