package com.lpro.bob.interfaz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.lpro.bob.R;

public class AyudaActivity extends AppCompatActivity {
    private WebView mWebView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        mWebView = (WebView) findViewById(R.id.webview);

        // Activo JavaScript
        mWebView.getSettings().setJavaScriptEnabled(true);

        // Cargamos la url que necesitamos
        mWebView.loadUrl("http://qubitia.com");
    }

    /**
     * Si dentro de nuestra app pulsamos un enlace se nos abrirá el navegador. Para evitarlo:
     */
    private class verMiWeb extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
