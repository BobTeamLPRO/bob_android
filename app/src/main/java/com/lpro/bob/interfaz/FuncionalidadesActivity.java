package com.lpro.bob.interfaz;

import android.app.Activity;
import android.preference.PreferenceFragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.lpro.bob.R;

public class FuncionalidadesActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new FuncionalidadesFragment())
                .commit();
    }
}
