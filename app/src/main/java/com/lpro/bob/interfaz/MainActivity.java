package com.lpro.bob.interfaz;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TableRow;

import com.lpro.bob.R;
import com.lpro.bob.persistencia.BobSQLiteHelper;
import com.lpro.bob.persistencia.Contrato;

public class MainActivity extends AppCompatActivity {
    TableRow funcionalidades, dispositivos, ajustes, actualizaciones, desarrollador, ayuda, about;
    Intent intent;
    private SQLiteDatabase database;
    private BobSQLiteHelper bobSQLiteHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("sharedPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("ip", "192.168.2.5");
        editor.putInt("port", 9998);
        editor.commit();


        bobSQLiteHelper = new BobSQLiteHelper(getApplicationContext());
        database = bobSQLiteHelper.getWritableDatabase();
        String query = "SELECT " + Contrato.Dispositivos.MAC + " FROM dispositivos WHERE " + Contrato.Dispositivos.TIPO + " = \"Bob\"";
        Cursor cursor = database.rawQuery(query, null);

       /* if (!cursor.moveToFirst()) {
            intent = new Intent(MainActivity.this, ConfiguracionActivity.class);
            startActivity(intent);
        }
        */
        //new ReceiveUDP(getApplicationContext()).execute();
        setContentView(R.layout.activity_main);
        funcionalidades = (TableRow) findViewById(R.id.tableRow1);
        dispositivos = (TableRow) findViewById(R.id.tableRow2);
        ajustes = (TableRow) findViewById(R.id.tableRow3);
        actualizaciones = (TableRow) findViewById(R.id.tableRow4);
        desarrollador = (TableRow) findViewById(R.id.tableRow5);
        ayuda = (TableRow) findViewById(R.id.tableRow6);
        about = (TableRow) findViewById(R.id.tableRow7);

        funcionalidades.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MainActivity.this, FuncionalidadesActivity.class);
                startActivity(intent);
            }
        });

        dispositivos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MainActivity.this, DispositivosActivity.class);
                startActivity(intent);
            }
        });

        ajustes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MainActivity.this, AjustesActivity.class);
                startActivity(intent);
            }
        });

        actualizaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MainActivity.this, ActualizacionesActivity.class);
                startActivity(intent);
            }
        });

        desarrollador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MainActivity.this, DesarrolladorActivity.class);
                startActivity(intent);
            }
        });

        ayuda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MainActivity.this, AyudaActivity.class);
                startActivity(intent);
            }
        });

        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MainActivity.this, AboutActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
