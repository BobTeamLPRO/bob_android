package com.lpro.bob.interfaz;

import android.content.Context;
import android.content.DialogInterface;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.lpro.bob.R;
import com.lpro.bob.comunicacion.Receive;
import com.lpro.bob.comunicacion.SendToBob;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ConfiguracionActivity extends AppCompatActivity {

    private final static String ssid = "BOBLPRO2016";
    private final static String psk = "2016LPROBOB";
    private ArrayList<String> scannedSSID;
    private ArrayList<String> scannedSecurity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracion);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        configureBob();

    }

    void configureBob() {
        WifiConfiguration conf = new WifiConfiguration();
        conf.SSID = "\"" + ssid + "\"";
        conf.preSharedKey = "\"" + psk + "\"";

        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        wifiManager.addNetwork(conf);
        List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
        Log.d("Conectivity", "List is: " + list);
        if (list != null) {
            for (WifiConfiguration i : list) {
                if (i.SSID != null && i.SSID.equals("\"" + ssid + "\"")) {
                    wifiManager.disconnect();
                    while (!wifiManager.enableNetwork(i.networkId, true)) {
                        wifiManager.enableNetwork(i.networkId, true);
                    }
                    wifiManager.reconnect();
                    break;
                }
            }

            try {
                Thread.sleep(5000);
            } catch (Exception e) {
                e.printStackTrace();
            }

            String ip;
            do {
                ip = getIPAddress();
                Log.d("Conectivity", "MY IP ADDRESS IS: " + ip + " with length " + ip.split("\\.").length);
            } while (ip.split("\\.").length != 4);


            //String ipBob = ip.replace(ip.split("\\.")[3], "122");
            String ipBob = "192.168.2.5";
            String message = "app*connect*" + wifiManager.getConnectionInfo().getMacAddress();
            (new SendToBob(ipBob, 9994, message)).execute();
            Log.d("Conectivty", "Sent: " + message + " to:" + ipBob);
            Receive receive = new Receive(getApplicationContext());
            receive.execute();

        } else {
            Toast toast = Toast.makeText(getApplicationContext(), "Please, enable Wifi", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    public void notifyMessageReceived(String message) {
        String[] data = message.split("\\*");
        if (data[0].trim().equals("config")) {

            for (int i = 1; i < data.length; i++) {
                if (i % 2 != 0) {
                    scannedSSID.add(data[i].trim());
                } else {
                    scannedSecurity.add(data[i].trim());
                }
            }

            showSelectionDialog((String[]) scannedSSID.toArray());

        } else {
            Log.d("configuracion", "Parsing message error");
        }
    }

    private String getIPAddress() {

        InetAddress ourIP = null;

        try {

            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());

            for (NetworkInterface intf : interfaces) {

                for (InterfaceAddress interfaceAddress : intf.getInterfaceAddresses()) {

                    InetAddress auxIP = interfaceAddress.getAddress();
                    ourIP = null;

                    if ((auxIP instanceof Inet4Address) && !auxIP.isLoopbackAddress()) {

                        ourIP = auxIP;

                        Log.d("Addresses", "IP: " + ourIP.getHostAddress());

                    }
                }

            }
            if (ourIP != null) {
                return ourIP.getHostAddress();
            }

        } catch (Exception e) {

            e.printStackTrace();

        }
        return "";
    }

    public void showSelectionDialog(String[] items) {
        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setTitle("Selecciona tu red");
        b.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                switch (which) {

                }
            }

        });

        b.show();
    }

}



