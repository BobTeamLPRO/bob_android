package com.lpro.bob.interfaz;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.lpro.bob.R;


/**
 * Adaptador con un cursor para poblar la lista de alquileres desde SQLite
 */
public class DispositivosAdapter extends RecyclerView.Adapter<DispositivosAdapter.ViewHolder> {
    private final Context contexto;
    private Cursor items;

    private OnItemClickListener escucha;

    interface OnItemClickListener {
        public void onClick(ViewHolder holder, String idDispositivo);
    }

    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        // Referencias UI
        public TextView viewMac;
        public TextView viewUbicacion;
        public TextView viewTipo;

        public ViewHolder(View v) {
            super(v);
            viewUbicacion = (TextView) v.findViewById(R.id.ubicacion);
            viewMac = (TextView) v.findViewById(R.id.mac);
            viewTipo = (TextView) v.findViewById(R.id.tipo);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            escucha.onClick(this, obtenerIdAlquiler(getAdapterPosition()));
        }
    }

    private String obtenerIdAlquiler(int posicion) {
        if (items != null) {
            if (items.moveToPosition(posicion)) {
                return items.getString(ConsultaAlquileres.ID_DISPOSITVO);
            }
        }

        return null;
    }

    public DispositivosAdapter(Context contexto, OnItemClickListener escucha) {
        this.contexto = contexto;
        this.escucha = escucha;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_lista_dispositivos, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        items.moveToPosition(position);

        String s;

        // Asignación UI
        s = items.getString(ConsultaAlquileres.NOMBRE);
        holder.viewMac.setText(s);

        s = items.getString(ConsultaAlquileres.MAC);
        holder.viewUbicacion.setText(s);

        s = items.getString(ConsultaAlquileres.TIPO);
        holder.viewTipo.setText(s);

    }

    @Override
    public int getItemCount() {
        if (items != null)
            return items.getCount();
        return 0;
    }

    public void swapCursor(Cursor nuevoCursor) {
        if (nuevoCursor != null) {
            items = nuevoCursor;
            notifyDataSetChanged();
        }
    }

    public Cursor getCursor() {
        return items;
    }

    interface ConsultaAlquileres {
        int ID_DISPOSITVO = 1;
        int NOMBRE = 2;
        int MAC = 3;
        int TIPO = 4;
    }
}
