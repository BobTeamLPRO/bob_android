package com.lpro.bob.interfaz;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.lpro.bob.R;

public class ActualizacionesActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnIniciar, btnDetener;
    private ImageView ivAnimacion;
    private AnimationDrawable savingAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actualizaciones);

        btnIniciar = (Button)findViewById(R.id.btn_iniciar_animacion);
        btnDetener = (Button)findViewById(R.id.btn_detener_animacion);

        btnIniciar.setOnClickListener(this);
        btnDetener.setOnClickListener(this);

        ivAnimacion = (ImageView)findViewById(R.id.iv_animacion);
        ivAnimacion.setBackgroundResource(R.drawable.animacion);
        savingAnimation = (AnimationDrawable)ivAnimacion.getBackground();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_iniciar_animacion:
                savingAnimation.start();
                new CountDownTimer(2500, 2500) {

                    public void onTick(long millisUntilFinished) {}

                    public void onFinish() {
                       savingAnimation.stop();
                        Toast toast = Toast.makeText(getApplicationContext(), "No se han encontrado nuevas actulizaciones", Toast.LENGTH_LONG);
                        toast.show();
                    }
                }.start();
                break;
            case R.id.btn_detener_animacion:
                savingAnimation.stop();
                break;
        }
    }
}
