package com.lpro.bob.interfaz;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.lpro.bob.R;

import java.io.OutputStream;
import java.net.Socket;

public class DesarrolladorActivity extends AppCompatActivity {
    Button serviceButton, stopServiceButton, mailButton, messageButton, configureButton;
    EditText editTextIP, editTextPort, editTextSend;

    Intent serviceServerIntent;
    Intent serviceHelperIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_desarrollador);

        serviceButton = (Button) findViewById(R.id.serviceButton);
        stopServiceButton = (Button) findViewById(R.id.stopServiceButton);
        mailButton = (Button) findViewById(R.id.mailButton);
        messageButton = (Button) findViewById(R.id.messageButton);
        configureButton = (Button) findViewById(R.id.configButton);

        editTextIP = (EditText) findViewById(R.id.editTextIP);
        editTextPort = (EditText) findViewById(R.id.editTextPort);
        editTextSend = (EditText) findViewById(R.id.editTextSend);


        serviceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                serviceHelperIntent = new Intent(getApplicationContext(), com.lpro.bob.comunicacion.BobHelper.class);
                startService(serviceHelperIntent);
                Log.i("Startup", "Se va a ejecutar el servicio BobHelper");
                serviceServerIntent = new Intent(getApplicationContext(), com.lpro.bob.comunicacion.BobServer.class);
                startService(serviceServerIntent);
                Log.i("Startup", "Se va a ejecutar el servicio BobServer");

            }
        });

        stopServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                stopService(serviceServerIntent);
                Log.i("Stop", "Se ha detenido el servicio BobServer");

            }
        });

        mailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DesarrolladorActivity.this, SendMailActivity.class);
                startActivity(intent);
            }
        });

        messageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SendToBob2(editTextIP.getText().toString(), Integer.parseInt(editTextPort.getText().toString()), editTextSend.getText().toString()).execute();
            }
        });

        configureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPref = getSharedPreferences("sharedPreferences", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("ip", editTextIP.getText().toString());
                editor.putInt("port", Integer.parseInt(editTextPort.getText().toString()));
                editor.commit();
                String ip = sharedPref.getString("ip", "0.0.0.0");
                int port = sharedPref.getInt("port", 9999);
                Log.d("DesarrolladorActivty", "IP: " + ip + ". Port: " + port);
            }
        });

    }

    public class SendToBob2 extends AsyncTask<Void, Void, Integer> {
        private String ip;
        private int port;
        private String textToSend;
        private Socket connectSocket;

        public SendToBob2(String textToSend) {
            this.textToSend = textToSend;
            SharedPreferences sharedPref = getSharedPreferences("sharedPreferences", Context.MODE_PRIVATE);
            ip = sharedPref.getString("ip", "0.0.0.0");
            port = sharedPref.getInt("port", 9999);
        }

        public SendToBob2(String ip, int port, String texttosend) {
            super();
            this.ip = ip;
            this.port = port;
            this.textToSend = texttosend;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            try {
                Log.d("IP", ip);
                Log.d("Puerto", String.valueOf(port));
                connectSocket = new Socket(ip, port); // Establecemos el Socket
                Log.d("Aquí", "Conectado");
                if (connectSocket.isConnected()) { // Si conseguimos conectarnos
                    OutputStream flujo = connectSocket.getOutputStream();
                    flujo.write(textToSend.getBytes());
                } else {
                    Log.d("SendToBob", "No estamos conectados");
                }
            } catch (Exception e) {
                Log.d("Excepcion", "Ha ocurrido una excepcion: " + e);
            }

            return 1;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
        }
    }
}
